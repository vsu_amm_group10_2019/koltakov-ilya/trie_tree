﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*12. Задано Trie-дерево и некоторое слово. Вывести все формы данного слова,
содержащиеся в дереве. Формой слова считается некоторое другое слово,
отличающееся от заданного не более чем на n последних символов, где n
вводит пользователь.*/

namespace Trie_Tree
{
    public partial class Form1 : Form
    {
        public Tree Tree = new Tree();
        public Form1()
        {
            InitializeComponent();
        }

        private void Redraw()
        {
            treeview.Nodes.Clear();
            Tree.PrintToTreeView(treeview);
            treeview.ExpandAll();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string fileName = openFileDialog1.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FindForm findForm = new FindForm(Tree);
            findForm.ShowDialog();
            
        }

        private void AddWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddDeleteWordForm addform = new AddDeleteWordForm(Tree);
            addform.ShowDialog();
            if (addform.OK)
            {
                Tree.Add(addform.word);
                Redraw();
            }
        }

        private void DeleteWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddDeleteWordForm deleteform = new AddDeleteWordForm(Tree);
            deleteform.ShowDialog();
            if (deleteform.OK)
            {
                Tree.Delete(deleteform.word);
                Redraw();
            }
        }
    }
}
