﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trie_Tree
{
    public class Tree
    {
        public Node Root { get; set; } = new Node();
        public void Add(string value)
        {
            Root.Add(value.ToLower());
        }
        public List<string> Find(string value, int n)
        {
            return Root.Find(value.ToLower(), "", n);
        }
        public Dictionary<string, int> Calculate()
        {
            return Root.Calculate("");
        }
        public void PrintToTreeView(TreeView treeView)
        {
            if (Root != null)
            {
                treeView.Nodes.Add("");
                Root.PrintToTreeNode(treeView.Nodes[0]);
            }
        }
        public void Delete(string word)
        {
            if (Root != null)
            {
                Root.Delete(word.ToLower());
            }
        }
    }
}
