﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trie_Tree
{
    public class Node
    {
        public Dictionary<char, Node> Childs { get; set; } = new Dictionary<char, Node>();
        public int Count { get; set; }
        public void Add(string value)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Childs.ContainsKey(next))
            {
                Node node = new Node();
                Childs.Add(next, node);
            }
            Childs[next].Add(remaining);
        }


        public bool IsFormOfWord(string value, string new_value, int n)
        {
            bool OK = false;
            if (value.Length == n)
            {
                OK = new_value.Length == n;
            }
            else
            {
                if (new_value.Length >= value.Length && new_value.Length <= value.Length + n)
                {
                    OK = true;
                    int i = 0;
                    while (OK && i <= value.Length - n)
                    {
                        if (new_value[i] == value[i])
                            i++;
                        else
                            OK = false;
                    }
                }
            }
            return OK;
        }

        public List<string> Find(string basic, string current,int  n)
        {
            List<string> result = new List<string>();
            int pos = current.Length;
            if (pos < basic.Length - n)
            {
                if (Childs.ContainsKey(basic[pos]))
                {
                    result.AddRange(Childs[basic[pos]].Find(basic, current + basic[pos].ToString(), n));
                }
                
            }
            else
            {
                if (current.Length >= basic.Length - n && current.Length <= basic.Length + n)
                {
                    if (Count > 0 && IsFormOfWord(basic, current, n))
                        result.Add(current);

                    foreach (var keyValuePair in Childs)
                    {
                        char next = keyValuePair.Key;
                        result.AddRange(Childs[next].Find(basic, current + next, n));
                    }
                }
              
            }
            return result;
        }

       
        public Dictionary<string, int> Calculate(string start)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            if (Count > 0)
            {
                result.Add(start, Count);
            }
            foreach (var keyValuePair in Childs)
            {
                Dictionary<string, int> tmp = keyValuePair.Value.Calculate(start + keyValuePair.Key);
                foreach (var kvp in tmp)
                {
                    result.Add(kvp.Key, kvp.Value);
                }
            }
            return result;
        }
        public void PrintToTreeNode(TreeNode treeNode)
        {
            int i = 0;
            foreach (var keyValuePair in Childs)
            {
                treeNode.Nodes.Add(keyValuePair.Key.ToString());
                keyValuePair.Value.PrintToTreeNode(treeNode.Nodes[i]);
                i++;
            }
        }
        public bool Delete(string word)
        {
            if (word == "")
            {
                if (Childs.Count != 0)
                {
                    Count = 0;
                    return false;
                }
                return true;
            }
            else
            {
                char next = word[0];
                string remaining = word.Length == 1 ? "" : word.Substring(1);

                if (Childs.ContainsKey(next))
                {
                    if (Childs[next].Delete(remaining))
                    {
                        Childs.Remove(next);
                        if (Childs.Count > 0)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
