﻿namespace Trie_Tree
{
    partial class FindForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxWord = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView_Find = new System.Windows.Forms.DataGridView();
            this.Colomn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Find)).BeginInit();
            this.SuspendLayout();
            // 
            // NumericUpDown
            // 
            this.NumericUpDown.Location = new System.Drawing.Point(48, 227);
            this.NumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumericUpDown.Name = "NumericUpDown";
            this.NumericUpDown.Size = new System.Drawing.Size(120, 22);
            this.NumericUpDown.TabIndex = 0;
            this.NumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Дополнительные буквы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Само слово";
            // 
            // TextBoxWord
            // 
            this.TextBoxWord.Location = new System.Drawing.Point(48, 62);
            this.TextBoxWord.Name = "TextBoxWord";
            this.TextBoxWord.Size = new System.Drawing.Size(120, 22);
            this.TextBoxWord.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(176, 349);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 56);
            this.button1.TabIndex = 4;
            this.button1.Text = "Посчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView_Find
            // 
            this.dataGridView_Find.AllowUserToAddRows = false;
            this.dataGridView_Find.AllowUserToDeleteRows = false;
            this.dataGridView_Find.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Find.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Colomn1});
            this.dataGridView_Find.Location = new System.Drawing.Point(247, 32);
            this.dataGridView_Find.Name = "dataGridView_Find";
            this.dataGridView_Find.ReadOnly = true;
            this.dataGridView_Find.RowHeadersWidth = 51;
            this.dataGridView_Find.RowTemplate.Height = 24;
            this.dataGridView_Find.Size = new System.Drawing.Size(279, 268);
            this.dataGridView_Find.TabIndex = 5;
            // 
            // Colomn1
            // 
            this.Colomn1.HeaderText = "Слово";
            this.Colomn1.MinimumWidth = 6;
            this.Colomn1.Name = "Colomn1";
            this.Colomn1.ReadOnly = true;
            this.Colomn1.Width = 125;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(247, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Формы слов";
            // 
            // FindForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 417);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView_Find);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TextBoxWord);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NumericUpDown);
            this.Name = "FindForm";
            this.Text = "FindForm";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Find)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown NumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxWord;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView_Find;
        private System.Windows.Forms.DataGridViewTextBoxColumn Colomn1;
        private System.Windows.Forms.Label label3;
    }
}