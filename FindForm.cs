﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trie_Tree
{
    public partial class FindForm : Form
    {
        public int n = 0;
        public string word = "";
        public Tree f_tree;
        public FindForm(Tree Tree)
        {
            InitializeComponent();
            f_tree = Tree;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //Обработка
            n = Convert.ToInt32(NumericUpDown.Value);
            word = TextBoxWord.Text;
            if (n > word.Length || word=="")
            {
                MessageBox.Show("Введены некорректные данные");
                Close();
            }
            else
            {
                List<string> f_slov = new List<string>();
                f_slov = f_tree.Find(word, n);
                //Визуализация
                dataGridView_Find.RowCount = f_slov.Count;
                for (int i = 0; i < dataGridView_Find.RowCount; i++)
                    dataGridView_Find.Rows[i].Cells[0].Value = f_slov[i];
            }
        }
    }
}
