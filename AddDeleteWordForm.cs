﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trie_Tree
{
    public partial class AddDeleteWordForm : Form
    {
        public AddDeleteWordForm(Tree Tree)
        {
            InitializeComponent();            
        }

        public string word;
        public bool OK = false;

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                word = textBox1.Text;
                OK = true;
            }
            else
                MessageBox.Show("Вы ввели пустую строку");
            Close();
        }
    }
}
